
Compiling
=========
Build jar package::

  mvn package

Generate eclipse project files::

  mvn eclipse:eclipse


Running tests
=============

Run server with::

  mvn exec:java -Dexec.mainClass=ee.vait.akka.benchmark.AkkaServer

and the client app with::

  mvn exec:java -Dexec.mainClass=ee.vait.akka.benchmark.Application2
