package ee.vait.akka.benchmark;

public class AkkaMainApplication {
	
	// TODO init server
	
	// TODO init client
	
	
	// TODO implement send(int byteCount)
	
	private final boolean async;
	private Application2 client;
	private Application2Backend server;

	/**
	 * 
	 * @param async -- true if async, false if sync
	 */
	public AkkaMainApplication(boolean async) {
		this.async = async;
//		this.initialize();
	}

	public void initialize() {
//		System.out.println("init server");
//		this.server = new Application2Backend();
//		System.out.println(this.server.system);
		
		System.out.println("init client");
		this.client = new Application2();
		System.out.println(this.client.system);
	}

	public void teardown() {
		try {
			System.out.println("waiting 3sec before teardown..");
			Thread.currentThread().sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.client.system.shutdown();
//		this.server.system.shutdown();
	}

	public void send(int byteCount) throws Exception {
		// TODO Auto-generated method stub
		Message msg = buildDummyMessage(byteCount);
		if (async){
			this.client.sendAsync(msg);
		} else {
			Object object = this.client.sendSync(msg);
			System.out.println(object); // TODO to log
		}
		
	}
	
	private Message buildDummyMessage(int byteCount) {
		Message message = new Message();
		message.payload=new byte[byteCount];
		return message;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("Testing async...");
		test(true);
		Thread.currentThread().sleep(3000);
//		System.out.println("Testing sync...");
//		test(false);
		System.out.println("DONE.");
	}

	private static void test(boolean async) throws Exception {
		AkkaMainApplication application  = new AkkaMainApplication(async);
		application.initialize();

		long s = System.currentTimeMillis();
		application.send(1000*1000*100);
		System.out.println("Sent in " + (System.currentTimeMillis()-s)+"ms");

		s = System.currentTimeMillis();
		application.send(1000*1000*100);
		System.out.println("Sent in " + (System.currentTimeMillis()-s)+"ms");
		
		s = System.currentTimeMillis();
		application.send(1000*1000*100);
		System.out.println("Sent in " + (System.currentTimeMillis()-s)+"ms");
		
		s = System.currentTimeMillis();
		application.send(1000*1000*100);
		System.out.println("Sent in " + (System.currentTimeMillis()-s)+"ms");
		
		s = System.currentTimeMillis();
		application.send(1000*1000*100);
		System.out.println("Sent in " + (System.currentTimeMillis()-s)+"ms");
		
		
		System.out.println("DONE?");
		
		
		
		application.teardown();
	}

}
