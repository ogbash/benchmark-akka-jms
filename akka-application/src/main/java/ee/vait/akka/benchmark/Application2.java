package ee.vait.akka.benchmark;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import scala.concurrent.Future;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import akka.util.Timeout;
import akka.dispatch.*;

public class Application2 /* implements Bootable */{
	ActorSystem system;
	private ActorRef sender;
	private ActorRef receiver;
	private Object msg;

	public Application2() {
		// TODO Auto-generated constructor stub
		System.out.println("init app2"); 
		Config config = ConfigFactory
		 .load().getConfig("remotecreation");
		 
		this.system = ActorSystem.create("ClientApplication", config);
		 
		
		 
		System.out.println("System done: " + system);

		this.sender = system.actorOf(new Props(SendingActor.class), "sender");
		System.out.println("Actor 'sender' done: " + sender);

		this.receiver = system.actorOf(new Props(ReceivingAndReplyingActor.class), "receiver");
		System.out.println("Actor 'receiver' done: " + receiver);

	}

	public static void main(String[] args) throws Exception {
		Application2 app2 = new Application2();

		app2.runOnce();
		
		int rounds = 1000*1000*20;
		app2.runMany(rounds);
		
		
		app2.system.shutdown();
	}

	private void runMany(int rounds) throws Exception {
		// TODO Auto-generated method stub
		long s=System.currentTimeMillis();
		long lengthSoFar = 0;
		System.out.println("count\t" + "Time spent(ms)\tspeed");
		for (int i =0; i < rounds;i++) {
			String result = sendWithReply("foo bad message for FUTURE");
			if (result == null) result = "";
			
			if (i % 1000 == 0) {
				long l = Math.max(1,System.currentTimeMillis()-s);
				System.out.println(i +  "\t"+l + "\t" + ((i*1.0)/l));
			}
			
			lengthSoFar+=result.length();
		}
		  System.out.println("sent and received " + lengthSoFar + " characters.");
		  long e = System.currentTimeMillis();
		  long spent = e-s;
		  System.out.println("Time spent "+spent + "ms");
		  
	}

	public void runOnce() throws Exception {
//		System.out.println("Sending single message to receiver");
//		this.receiver.tell("foo hau helllooooo", sender);

		  String result = sendWithReply("foo bad message for FUTURE");
		  System.out.println("result: " + result);
	}
	
	public void sendAsync(Message msg){
		receiver.tell(msg, sender);
	}
	
	public Object sendSync(Message msg) throws Exception{
		Timeout timeout = new Timeout(Duration.create("10 s").toMillis());
		System.out.println("sync sending");
		Future<Object> future = Patterns.ask(receiver,msg , timeout);
		Object result =  Await.result(future, timeout.duration());
		System.out.println("sync done");
		return result;
	}

	@Deprecated
	public String sendWithReply(String msg) throws Exception {
		Timeout timeout = new Timeout(Duration.create("1 hours").toMillis());
//		System.out.println(timeout);
		Future<Object> future = Patterns.ask(receiver,msg , timeout);
		String result = (String) Await.result(future, timeout.duration());
		return result;
	}

}
