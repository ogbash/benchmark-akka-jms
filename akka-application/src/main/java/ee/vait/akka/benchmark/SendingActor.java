package ee.vait.akka.benchmark;


import ee.vait.akka.benchmark.ReceivingAndReplyingActor.BadRequest;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class SendingActor extends UntypedActor {
	
	LoggingAdapter log =  Logging.getLogger(getContext().system(), this);
	
	
	public static class Response{
		
	}
	
	@Override
	public void onReceive(Object o) throws Exception {
		if (o instanceof BadRequest) {
			log.info("received [" + ((BadRequest)o).o+"]");
		} else if (o instanceof Response) {
			Response r = (Response)o;
			
		}
	}

}
