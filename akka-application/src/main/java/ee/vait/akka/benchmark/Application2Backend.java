package ee.vait.akka.benchmark;

import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.kernel.Main;

public class Application2Backend {

	ActorSystem system;
	private ActorRef sender;
	private ActorRef receiver;
	private Object msg;

	public Application2Backend() {
		// This actor system will host ReceivingAndReplyingActor that is instantiated from client side 
		 this.system = ActorSystem.create("AkkaServer", ConfigFactory
		 .load().getConfig("serverApp"));
//		 this.system=ActorSystem.create("ServerApplication");
	}
	
	public static void main(String[] args) {
		Application2Backend application2Backend = new Application2Backend();
		
		
//		application2Backend.system.shutdown();
		
	}
	
}
