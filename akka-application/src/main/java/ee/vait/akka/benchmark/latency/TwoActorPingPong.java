package ee.vait.akka.benchmark.latency;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Address;
import akka.actor.AddressFromURIString;
import akka.actor.Deploy;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.pattern.Patterns;
import akka.remote.RemoteScope;
import akka.serialization.JSerializer;
import akka.util.Timeout;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * Creates two actors: Requestor and Responder and measures latency between
 * them. The second actor may be a future.
 * 
 * @author olegus
 */
public class TwoActorPingPong {
	// messages
	/** Say requestor to start the ping-pong process. */
	@SuppressWarnings("serial")
	static class Start implements Serializable {
		public ActorRef responder;
		public Start(ActorRef responder) {
			this.responder = responder;
		}
	}
	
	/** Make a request. */
	@SuppressWarnings("serial")
	static class Ping implements Serializable {}
	
	/** Responder sends this message back. */
	@SuppressWarnings("serial")
	static class Pong implements Serializable {}
	
	/** Test finished as notified by requestor. */
	@SuppressWarnings("serial")
	static class Finished implements Serializable {}
	
	/**
	 * Request actor.
	 * @author olegus
	 */
	static class Requestor extends UntypedActor {
		ActorRef initiator = null;
		ActorRef responder = null;
		int count = 0;
		int MEASURE_COUNT = 1000;
		int TOTAL_COUNT = 30*1000;
		long startTime;
		long prevTime;

		@Override
		public void onReceive(Object m) throws Exception {
			if (responder==null && m instanceof Start) {
				// start
				initiator = this.sender();
				responder = ((Start)m).responder;
				startTime =  System.currentTimeMillis();
				prevTime = startTime;
				responder.tell(new Ping(), self());
				
			} else if (m instanceof Pong) {
				count ++;
				// print time
				if (count%MEASURE_COUNT == 0) {
					long lapse = System.currentTimeMillis()-prevTime;
					System.out.println(count+" " + ((double)lapse/MEASURE_COUNT));
					prevTime = System.currentTimeMillis();
				}
				
				// react
				if (count==TOTAL_COUNT) {
					// finish
					initiator.tell(new Finished(), self());
				} else {
					// send new ping
					responder.tell(new Ping(), self());
				}
			}
		}
	}
	
	/**
	 * Respond actor.
	 * @author olegus
	 */
	static class Responder extends UntypedActor {
		@Override
		public void onReceive(Object m) throws Exception {
			if (m instanceof Ping) {
				this.sender().tell(new Pong(), self());
			}
		}

		@Override
		public void postStop() throws Exception {
			super.postStop();
			System.err.println("Responder stopped");
		}
	}

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		boolean isRemote = true;
		
		ActorSystem actorSystem = null;
		ActorRef requestor = null;
		ActorRef responder = null;
		
		// create actors
		if (!isRemote) {
			actorSystem = ActorSystem.create("AkkaMain");
			requestor = actorSystem.actorOf(Props.create(Requestor.class), "requestor");
			responder = actorSystem.actorOf(Props.create(Responder.class));
			
		} else {
			Config config = ConfigFactory.load().getConfig("remotelookup");
			actorSystem = ActorSystem.create("AkkaMain", config);
			requestor = actorSystem.actorOf(Props.create(Requestor.class), "requestor");
			Address address = AddressFromURIString.parse("akka.tcp://AkkaServer@127.0.0.1:2552");
			System.err.println("Deploying remotely on "+address);
			responder = actorSystem.actorOf(Props.create(Responder.class).withDeploy(
					new Deploy(new RemoteScope(address))), "responder");
		}
		
		// wait for results
		Future<Object> future = Patterns.ask(requestor, new Start(responder), new Timeout(5,TimeUnit.MINUTES));
		Await.result(future, Duration.Inf());
		
		// shut down
		actorSystem.shutdown();
		System.err.println("Exiting");
	}

	/** Custom serializer. */
    static public class Serializer extends JSerializer {
        
    	public Serializer() {
    	}
    	
		// This is whether "fromBinary" requires a "clazz" or not
		@Override
		public boolean includeManifest() {
			return false;
		}

		// Pick a unique identifier for your Serializer,
		// you've got a couple of billions to choose from,
		// 0 - 16 is reserved by Akka itself
		@Override
		public int identifier() {
			return 4831;
		}

		static byte[] ping = new byte[1];
		static byte[] pong = new byte[1];
		static { ping[0] = 1; pong[0] = 2; }
		
		// "toBinary" serializes the given object to an Array of Bytes
		@Override
		public byte[] toBinary(Object obj) {
			// Put the code that serializes the object here
			if (obj instanceof Ping) return ping;
			else if (obj instanceof Pong) return pong;
			else throw new RuntimeException("Do not know how to serialize "+obj);
		}

		// "fromBinary" deserializes the given array,
		// using the type hint (if any, see "includeManifest" above)
		@Override
		public Object fromBinaryJava(byte[] bytes, Class<?> clazz) {
			// Put your code that deserializes here
			if (bytes[0] == 1) return new Ping();
			else if (bytes[0] == 2) return new Pong();
			else throw new RuntimeException("Do not know how to deserialize "+bytes[0]);
		}
	}
}
