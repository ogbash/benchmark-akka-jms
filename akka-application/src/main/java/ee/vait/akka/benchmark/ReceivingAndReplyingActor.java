package ee.vait.akka.benchmark;

import akka.actor.ActorRef; 
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class ReceivingAndReplyingActor extends UntypedActor {

	public static class BadRequest {
		public final Object o;

		public BadRequest(Object o) {
			this.o = o;
		}
	}
	
	public static class Request {
		public final long created;

		public Request() {
			// TODO Auto-generated constructor stub
			this.created=System.nanoTime();
		}
	}
	
	LoggingAdapter log =  Logging.getLogger(getContext().system(), this);
	
	@Override
	public void onReceive(Object o) throws Exception {
		ActorRef sender = getSender();
		
		if (o instanceof Request) {
			Request r = (Request) o;
			
		}
		else if(o instanceof Message) {
			Message m = (Message) o;
			System.out.println("Received message with payload length " + m.payload.length);
			sender.tell("ok " + m.payload.length, getSelf());
		}
		else {
//			System.out.println("received unrecognized request ["+o+"], trying to reply to" + sender);
			sender.tell("Bad request:" + o, getSelf());
		}
		
	}

}
