package ee.vait.akka.benchmark;

import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;
import akka.kernel.Bootable;
import akka.kernel.Main;
/**
 * Backend node. This node will have its actors deployed from remote location. 
 * See configuration file "application.conf" under section "serverApp"
 * @author mativait
 *
 */
public class AkkaServer implements Bootable {

	private ActorSystem system;

	public static void main(String[] args) {
		Main.main(new String[]{"ee.vait.akka.benchmark.AkkaServer"});
		System.out.println("Press CTRL-C for exit...");
	}

	@Override
	public void shutdown() {
		this.system.shutdown();
	}

	@Override
	public void startup() {
		long s = System.currentTimeMillis();
		this.system = ActorSystem.create("AkkaServer", ConfigFactory.load()
				.getConfig("serverApp"));
		System.out.println("server init done in " + (System.currentTimeMillis()-s) + "ms.");
	}

}
