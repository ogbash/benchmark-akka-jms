import numpy as np
import matplotlib.pyplot as plt

pps = [
	('ping-pong', np.loadtxt("pp.result")),
	('pp serial-own', np.loadtxt("pp-own.result")),
	('pp nodelay-off', np.loadtxt("pp-nagle.result"))
]

for name,data in pps:
	plt.plot(data[:,0], data[:,1], label=name)

plt.legend()
plt.show()

