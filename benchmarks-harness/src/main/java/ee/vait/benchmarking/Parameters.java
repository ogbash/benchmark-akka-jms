package ee.vait.benchmarking;
/**
 * General parameters used for benchmarking
 * @author mativait
 *
 */
public class Parameters {
	static final int iterationCount = 2;
	static final int time = 1000; // 1 sec
	static final int forkCount = 1;
	}
	