package ee.vait.benchmarking;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.ForkedMain;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.GenerateMicroBenchmark;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;

import ee.vait.akka.benchmark.AkkaMainApplication;
import ee.vait.akka.benchmark.AkkaServer;

@State(Scope.Benchmark)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = Parameters.iterationCount, time = Parameters.time, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = Parameters.iterationCount, time = Parameters.time, timeUnit = TimeUnit.MILLISECONDS)
@Fork(Parameters.forkCount)
public class AkkaAsyncBenchmark extends AbstractBenchmarkable{

	public AkkaAsyncBenchmark() {
		System.out.println("Akka async benchmark...");
	}

	private AkkaMainApplication application;

	public void i2(){
		System.out.println("hmmm");
		try {
			
//			AkkaServer.main(null);

			System.out.println("cool");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("sht");
		}
		
	}
	
	
	
	@Setup
	public void init() {
		System.out.println("init...");
		boolean async=true;
			this.application = new AkkaMainApplication(async);
		application.initialize();
		try {
			Thread.currentThread().sleep(2000);
			System.out.println("init done.");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void send(int size) {
		try {			
			this.application.send(size);
		} catch (Exception e) {
			e.printStackTrace();
//			throw new Error("Send failed");
			init();
		}
	}

	@TearDown
	public void teardown() {
		this.application.teardown();
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send1Byte(){
		send(1);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send10Byte(){
		send(10);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public  void send100Byte(){
		send(100);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public  void send1000Byte(){
		send(1000);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send1000000Byte(){
		send(1000000);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send10000000Byte(){
		send(10000000);
	}

	public static void main(String[] args) {
		System.out.println(AkkaSyncBenchmark.class.toString());
	}
}
