package ee.vait.benchmarking;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.GenerateMicroBenchmark;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;
import org.springframework.jms.core.JmsTemplate;

import ee.vait.jms.app.Application;

@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = Parameters.iterationCount, time = Parameters.time, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = Parameters.iterationCount, time = Parameters.time, timeUnit = TimeUnit.MILLISECONDS)
@Fork(Parameters.forkCount)
public class JmsSyncBenchmark extends AbstractBenchmarkable{


	private Application a;
	private JmsTemplate jmsTemplate;

	@Setup
	public void init() {
		System.out.println("setting up " + this.getClass());
		this.a = new Application();
		
		this.jmsTemplate = a.initialize();
	}
	
	@Override
	public void send(int size) {
		jmsTemplate.send(Application.mailboxDestination, Application.payload(size));
	}

	@TearDown
	public void teardown() {
		a.tearDown();
		System.out.println("teardown done.");
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send1Byte(){
		send(1);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send10Byte(){
		send(10);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public  void send100Byte(){
		send(100);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public  void send1000Byte(){
		send(1000);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send1000000Byte(){
		send(1000000);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send10000000Byte(){
		send(10000000);
	}

}
