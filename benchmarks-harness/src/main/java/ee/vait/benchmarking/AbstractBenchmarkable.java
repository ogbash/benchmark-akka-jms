package ee.vait.benchmarking;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.GenerateMicroBenchmark;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;


@State(Scope.Thread)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = Parameters.iterationCount, time = Parameters.time, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = Parameters.iterationCount, time = Parameters.time, timeUnit = TimeUnit.MILLISECONDS)
@Fork(Parameters.forkCount)
public abstract class AbstractBenchmarkable implements  Benchmarkable{
	
	public abstract void send(int byteCount);

	
	

}
