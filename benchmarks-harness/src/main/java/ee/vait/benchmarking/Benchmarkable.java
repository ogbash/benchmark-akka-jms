package ee.vait.benchmarking;

import org.openjdk.jmh.annotations.Setup;

public interface Benchmarkable {

	@Setup
	public void init();
	public void send(int byteCount);
	
}
