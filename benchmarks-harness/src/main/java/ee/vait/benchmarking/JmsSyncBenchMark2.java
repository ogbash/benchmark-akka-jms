package ee.vait.benchmarking;

import java.util.concurrent.TimeUnit;

import javax.jms.JMSException;

import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.GenerateMicroBenchmark;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;

import ee.vait.jms.app.v3.RawAsyncActiveMQClient;

@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = Parameters.iterationCount, time = Parameters.time, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = Parameters.iterationCount, time = Parameters.time, timeUnit = TimeUnit.MILLISECONDS)
@Fork(Parameters.forkCount)
public class JmsSyncBenchMark2 extends AbstractBenchmarkable{


	RawAsyncActiveMQClient c;
	@Setup
	public void init() {
		try {
			c = new RawAsyncActiveMQClient();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("sending");
	}
	
	@Override
	public void send(int size) {
		try {
			c.send(new byte[size]);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@TearDown
	public void teardown() {
		System.out.println("Starting teardown " + getClass());
		c.shutdown();
		System.out.println("teardown done.");
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send1Byte(){
		send(1);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send10Byte(){
		send(10);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public  void send100Byte(){
		send(100);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public  void send1000Byte(){
		send(1000);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send1000000Byte(){
		send(1000000);
	}

	@GenerateMicroBenchmark
	@BenchmarkMode(Mode.All)
	public void send10000000Byte(){
		send(10000000);
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("init");
		RawAsyncActiveMQClient c = new RawAsyncActiveMQClient();
		System.out.println("sending");
		c.send(new byte[10]);
		System.out.println("sent");
		c.shutdown();
		System.out.println("Should exit now.");
	}
}
