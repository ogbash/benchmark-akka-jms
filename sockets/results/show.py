
import matplotlib.pyplot as plt
import numpy as np

plots = [
	('C',np.loadtxt('c/oleg/latency.dat')),
	('Java',np.loadtxt('java/oleg/latency.dat'))
	]

for name,data in plots:
	print name, np.mean(data[:,1])
	plt.plot(data[:,0],data[:,1], label=name)

plt.legend()
plt.show()
