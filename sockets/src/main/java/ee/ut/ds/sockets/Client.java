package ee.ut.ds.sockets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	private static int TOTAL_COUNT = 100*1000;
	private static int OUT_COUNT = 1000;

	/**
	 * @param args
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException, IOException {
		Socket s = new Socket("localhost",2222);
		byte[] bts = new byte[1];
		bts[0] = 1;
		InputStream is = s.getInputStream();
		OutputStream os = s.getOutputStream();
		long prevTime = System.currentTimeMillis();
		for (int i=0; i<TOTAL_COUNT ; i++) {
			os.write(bts);
			int c = is.read(bts);
			if (c<=0)
				break;
			if ((i+1)%OUT_COUNT==0) {
				long t = System.currentTimeMillis();
				System.out.println((i+1) + " " + (t-prevTime)/(double)OUT_COUNT);
				prevTime = t;
			}
		}
		s.close();

	}

}
