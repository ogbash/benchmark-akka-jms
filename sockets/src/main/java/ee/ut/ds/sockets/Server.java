package ee.ut.ds.sockets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class Server {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException, IOException {
		ServerSocket ss = new ServerSocket(2222, 3,
				Inet4Address.getByName("127.0.0.1"));
		try {
			Socket s = ss.accept();
			InputStream is = s.getInputStream();
			OutputStream os = s.getOutputStream();
			byte[] bts = new byte[1];
			// echo
			while (true) {
				int n = is.read(bts);
				if (n > 0) {
					os.write(bts);
				}
			}
		} finally {
			ss.close();
		}
	}

}
