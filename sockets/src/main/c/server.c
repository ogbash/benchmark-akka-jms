
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

#include <linux/tcp.h> // TCP_NODELAY

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     int flag = 1;
     int result = setsockopt(sockfd,          /* socket affected */
			     IPPROTO_TCP,     /* set option at TCP level */
			     TCP_NODELAY,     /* name of option */
			     (char *) &flag,  /* the cast is historical
						 cruft */
			     sizeof(int));    /* length of option value */
     if (result < 0)
        error("ERROR setting socket flags");

     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, 
                 (struct sockaddr *) &cli_addr, 
                 &clilen);
     if (newsockfd < 0) 
          error("ERROR on accept");

     while(1) {
       n = read(newsockfd,buffer,1);
       if (n < 0) error("ERROR reading from socket");
       n = write(newsockfd,"1",1);
       if (n < 0) error("ERROR writing to socket");
     }

     close(newsockfd);
     close(sockfd);
     return 0; 
}
