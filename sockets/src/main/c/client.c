#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
//#include <time.h>
#include <sys/time.h>

#include <linux/tcp.h> // TCP_NODELAY

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[256];
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
     int flag = 1;
     int result = setsockopt(sockfd,          /* socket affected */
			     IPPROTO_TCP,     /* set option at TCP level */
			     TCP_NODELAY,     /* name of option */
			     (char *) &flag,  /* the cast is historical
						 cruft */
			     sizeof(int));    /* length of option value */
     if (result < 0)
        error("ERROR setting socket flags");

    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");

    int TOTAL_COUNT=100*1000;
    int OUT_COUNT=1000;
    struct timeval start,prev,cur;
    gettimeofday(&start, NULL);
    prev = start;
    for (int i=0; i<TOTAL_COUNT; i++) {
      n = write(sockfd,buffer,1);
      if (n < 0) 
	error("ERROR writing to socket");
      n = read(sockfd,buffer,1);
      if (n < 0) 
	error("ERROR reading from socket");
      if ((i+1)%OUT_COUNT==0) {
	gettimeofday(&cur, NULL);
	double msec =  (cur.tv_sec-prev.tv_sec)*1000 + (cur.tv_usec-prev.tv_usec)/1000.0;
	printf("%d %.4f\n", i+1, msec/OUT_COUNT);
	prev = cur;
      }
    }
    close(sockfd);
    return 0;
}
