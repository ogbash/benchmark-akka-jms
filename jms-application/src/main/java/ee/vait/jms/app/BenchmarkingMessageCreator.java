package ee.vait.jms.app;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.springframework.jms.core.MessageCreator;

public class BenchmarkingMessageCreator implements MessageCreator {

	private int dummyMessageSize;

	public BenchmarkingMessageCreator(int messageSizeInBytes) {
		this.dummyMessageSize=messageSizeInBytes;
	}

	public Message createMessage(Session session) throws JMSException {
		// TODO Auto-generated method stub
		ee.vait.jms.app.Message m = new ee.vait.jms.app.Message();
		m.payload=new byte[this.dummyMessageSize];
		// message.writeInt(111);
		
		ObjectMessage message = session.createObjectMessage(m);
		
//		System.out.println("MSG: "+message);
		System.out.println("payload len="+m.payload.length);
		return message;
	}
}