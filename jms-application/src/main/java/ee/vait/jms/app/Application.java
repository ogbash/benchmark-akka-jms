package ee.vait.jms.app;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.listener.SimpleMessageListenerContainer;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.SimpleMessageConverter;

@Configuration
public class Application {

	public static String mailboxDestination = "mailbox-destination";
	private AnnotationConfigApplicationContext context;
	private JmsTemplate jmsTemplate;

	@Bean
	ConnectionFactory connectionFactory() {
		ActiveMQConnectionFactory targetConnectionFactory = new ActiveMQConnectionFactory(
				"tcp://localhost:61616");
		targetConnectionFactory.setAlwaysSessionAsync(true);
		targetConnectionFactory.setAlwaysSyncSend(false);
		CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(targetConnectionFactory);
		return cachingConnectionFactory;
	}

	@Bean
	MessageListener listener() {
		return new MessageListener() {
			
			public void onMessage(Message message) {
				// TODO Auto-generated method stub
				System.out.println("message: "+message.getClass());
			}
		};
		
	}
	
//	@Bean
//	MessageConverter messageConverter() {
//		return new MessageConverter() {
//
//			public Message toMessage(Object object, Session session)
//					throws JMSException, MessageConversionException {
//				// TODO Auto-generated method stub
//				System.out.println("TODO convert " + object.getClass());
//				return null;
//			}
//
//			public Object fromMessage(Message message) throws JMSException,
//					MessageConversionException {
//				// TODO Auto-generated method stub
////				Object
//				
//				ee.vait.jms.app.Message m = new ee.vait.jms.app.Message();
//				m.payload=new byte[(int) (System.currentTimeMillis() % message.toString().length()) ];
//				return m;
//			}
//		};
//			
//	}
	
	@Bean
	MessageListenerAdapter receiver() {
		return new MessageListenerAdapter(new Receiver()) {
			{
				setDefaultListenerMethod("receiveMessage");
			}
		};
	}

	@Bean
	SimpleMessageListenerContainer container(
			final MessageListenerAdapter messageListener,
			final ConnectionFactory connectionFactory) {
		return new SimpleMessageListenerContainer() {
			{
				setMessageListener(messageListener);
				setConnectionFactory(connectionFactory);
				setDestinationName(mailboxDestination);
			}
		};
	}

	@Bean
	JmsTemplate jmsTemplate(ConnectionFactory connectionFactory) {
		return new JmsTemplate(connectionFactory);
	}

	public static void main(String args[]) throws Throwable {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				Application.class);

		int messageSizeInBytes = 1024000;
		BenchmarkingMessageCreator messageCreator = new BenchmarkingMessageCreator(
				messageSizeInBytes);

		JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);

		send(messageCreator, jmsTemplate);

		context.close();
	}

	private static void send(BenchmarkingMessageCreator messageCreator,
			JmsTemplate jmsTemplate) {
		int rounds = 1;

		do
			jmsTemplate.send(mailboxDestination, messageCreator);
		while (rounds-- > 0);

	}

	public JmsTemplate initialize() {
		if (this.context != null){
			throw new Error("JMS already initialized!");
		}
		
		this.context= new AnnotationConfigApplicationContext(
				Application.class);
		this.jmsTemplate = context.getBean(JmsTemplate.class);
		
		return this.jmsTemplate;
	}

	public void tearDown(){
		this.jmsTemplate=null;
		this.context.close();
	}

	public static MessageCreator payload(int sizeInBytes) {
		return new BenchmarkingMessageCreator(sizeInBytes);
	}
		
		
		
}