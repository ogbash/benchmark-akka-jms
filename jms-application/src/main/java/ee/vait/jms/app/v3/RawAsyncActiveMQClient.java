package ee.vait.jms.app.v3;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RawAsyncActiveMQClient implements MessageListener {
	private static int ackMode;
	private static String clientQueueName;

	private boolean transacted = false;
	private MessageProducer producer;
	private long sent;
	private Destination retDest;
	private Session session;
	private MessageConsumer responseConsumer;

	static {
		clientQueueName = "client.messages";
		ackMode = Session.AUTO_ACKNOWLEDGE;
	}

	public RawAsyncActiveMQClient() throws InterruptedException {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				"tcp://localhost:61616?jms.useAsyncSend=true");
		Connection connection;
		try {
			connection = connectionFactory.createConnection();
			connection.start();
			this.session = connection.createSession(transacted, ackMode);
			Destination adminQueue = session.createQueue(clientQueueName);

			// Setup a message producer to send message to the queue the server
			// is consuming from
			this.producer = session.createProducer(adminQueue);
			this.producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

			// Create a temporary queue that this client will listen for
			// responses on then create a consumer
			// that consumes message from this temporary queue...for a real
			// application a client should reuse
			// the same temp queue for each message to the server...one temp
			// queue per client
			Destination tempDest = session.createTemporaryQueue();
			if (this.retDest == null)
				this.retDest = tempDest;
			this.responseConsumer = session.createConsumer(tempDest);
			// This class will handle the messages to the temp queue as well
			// responseConsumer.setMessageListener(this);

		} catch (JMSException e) {
			// Handle the exception appropriately
		}
	}

	private synchronized void send(Destination tempDest, TextMessage txtMessage)
			throws JMSException {

		// Now create the actual message you want to send

		// Set the reply to field to the temp queue you created above, this is
		// the queue the server
		// will respond to
		txtMessage.setJMSReplyTo(this.retDest);

		// Set a correlation ID so when you get a response you know which sent
		// message the response is for
		// If there is never more than one outstanding message to the server
		// then the
		// same correlation ID can be used for all the messages...if there is
		// more than one outstanding
		// message to the server you would presumably want to associate the
		// correlation ID with this
		// message somehow...a Map works good
		String correlationId = this.createRandomString();
		txtMessage.setJMSCorrelationID(correlationId);

		long s = System.currentTimeMillis();
		// System.out.println(s);
		this.producer.send(txtMessage);
		this.sent = s;

		Message message = responseConsumer.receive();

		if (message instanceof TextMessage) {
			try {
				boolean equals = correlationId.equals(message
						.getJMSCorrelationID());
				if (equals == false) {
					throw new Exception("Correlation ids do not match!");
				} else {
					TextMessage txtMsg = (TextMessage) message;
					String messageText = txtMsg.getText();
					System.out.println("time spent: "
							+ (System.currentTimeMillis() - s)
							+ "ms, msg length " + messageText.length()
							+ ", ids match? " + equals + " txt="+messageText);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private String createRandomString() {
		Random random = new Random(System.currentTimeMillis());
		long randomLong = random.nextLong();
		return Long.toHexString(randomLong);
	}

	public synchronized void onMessage(Message message) {
		String messageText = null;
		try {
			if (message instanceof TextMessage) {
				TextMessage textMessage = (TextMessage) message;
				messageText = textMessage.getText();
				System.out.println("messageText = " + messageText.length());
				long s = System.currentTimeMillis();
				System.out.println(s - sent);
				this.sent = s;
				System.out.println("T@recv:" + Thread.currentThread());
			}
		} catch (JMSException e) {
			// Handle the exception appropriately
		}
	}

	public static void main(String[] args) throws InterruptedException,
			JMSException {

		RawAsyncActiveMQClient c;
		try {
			c = new RawAsyncActiveMQClient();
			for (int j = 1; j < 1000000001; j *= 5) {

				for (int i = 0; i < 4; i++) {
					c.send(new byte[j]);
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void send(byte[] bs) throws JMSException {
		TextMessage txt = this.session.createTextMessage(new String(
				new char[bs.length]));
		// TODO Auto-generated method stub
		send(retDest, txt);
	}
}
