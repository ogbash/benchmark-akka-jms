package ee.vait.jms.app;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.listener.SimpleMessageListenerContainer;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;

/**
 * TODO cleanup this & {@link Application} class
 * @author mativait
 *
 */
@Configuration
public class AsyncApplication {

	public static String mailboxDestination = "mailbox-destination";
	private AnnotationConfigApplicationContext context;
	private JmsTemplate jmsTemplate;

	@Bean
	ConnectionFactory connectionFactory() {
		ActiveMQConnectionFactory targetConnectionFactory = new ActiveMQConnectionFactory(
				"tcp://localhost:61616?jms.useAsyncSend=true");
		targetConnectionFactory.setAlwaysSessionAsync(true);
		targetConnectionFactory.setAlwaysSyncSend(false);
		CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(targetConnectionFactory);
		return cachingConnectionFactory;
	}

	@Bean
	MessageListenerAdapter receiver() {
		return new MessageListenerAdapter(new Receiver()) {
			{
				setDefaultListenerMethod("receiveMessage");

			}
		};
	}

	@Bean
	SimpleMessageListenerContainer container(
			final MessageListenerAdapter messageListener,
			final ConnectionFactory connectionFactory) {
		return new SimpleMessageListenerContainer() {
			{
				setMessageListener(messageListener);
				setConnectionFactory(connectionFactory);
				setDestinationName(mailboxDestination);
			}
		};
	}

	@Bean
	JmsTemplate jmsTemplate(ConnectionFactory connectionFactory) {
		return new JmsTemplate(connectionFactory);
	}


	public static void main(String args[]) throws Throwable {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				AsyncApplication.class);

		int messageSizeInBytes = 1024000;
		BenchmarkingMessageCreator messageCreator = new BenchmarkingMessageCreator(
				messageSizeInBytes);

		JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);

		send(messageCreator, jmsTemplate);

		context.close();
	}

	private static void send(BenchmarkingMessageCreator messageCreator,
			JmsTemplate jmsTemplate) {
		int rounds = 1;

		do
			jmsTemplate.send(mailboxDestination, messageCreator);
		while (rounds-- > 0);

	}

	public JmsTemplate initialize() {
		if (this.context != null){
			throw new Error("JMS already initialized!");
		}
		
		this.context= new AnnotationConfigApplicationContext(
				AsyncApplication.class);
		this.jmsTemplate = context.getBean(JmsTemplate.class);
		
		return this.jmsTemplate;
	}

	public void tearDown(){
		this.jmsTemplate=null;
		this.context.close();
	}

	public static MessageCreator payload(int sizeInBytes) {
		return new BenchmarkingMessageCreator(sizeInBytes);
	}
		
		
		
}